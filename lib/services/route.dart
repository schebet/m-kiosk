import 'package:flutter/material.dart';
import 'package:read_app/data/models/category.dart' as model;
import 'package:read_app/data/models/product.dart' as product;
import 'package:read_app/data/models/shop.dart';
import 'package:read_app/screens/auth/auth_wrapper.dart';
import 'package:read_app/screens/auth/getstarted.dart';
import 'package:read_app/screens/cart.dart';
import 'package:read_app/screens/categories.dart';
import 'package:read_app/screens/home.dart';
import 'package:read_app/screens/product.dart';
import 'package:read_app/screens/products.dart';
import 'package:read_app/screens/profile.dart';
import 'package:read_app/screens/screen_wrapper.dart';
import 'package:read_app/screens/search.dart';
import 'package:read_app/screens/store.dart';
import 'package:read_app/screens/stores.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case '/':
      return MaterialPageRoute(builder: (context) => GetStarted());
    case '/screenwrap':
      return MaterialPageRoute(builder: (context) => ScreenWrapper());
    case '/auth':
      return MaterialPageRoute(builder: (context) => AuthWrapper());
    case '/home':
      return MaterialPageRoute(builder: (context) => Home());
    case '/store':
      Shops shops = settings.arguments as Shops;
      return MaterialPageRoute(builder: (context) => Store(shops: shops));
    case '/products':
      final arguments = settings.arguments as Map;
      Shops shops = arguments["shops"] as Shops;
      model.SubCategories subCategories =
          arguments["sub"] as model.SubCategories;
      return MaterialPageRoute(
          builder: (context) =>
              Products(shops: shops, subCategories: subCategories));
    case '/product':
      product.Products products = settings.arguments as product.Products;
      return MaterialPageRoute(
          builder: (context) => Product(products: products));
    case '/profile':
      return MaterialPageRoute(builder: (context) => Profile());
    case '/cart':
      return MaterialPageRoute(builder: (context) => Cart());
    case '/categories':
      return MaterialPageRoute(builder: (context) => Categories());
    case '/stores':
      return MaterialPageRoute(builder: (context) => Stores());
    case '/search':
      return MaterialPageRoute(builder: (context) => Search());
  }
}
