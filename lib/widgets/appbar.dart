import 'package:flutter/material.dart';
import 'package:read_app/widgets/cart.dart';
import 'package:read_app/widgets/change_theme.dart';

class CustomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: BackButton(),
      trailing: CartIcon(),
    );
  }
}

