import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 15, 8, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
            text: TextSpan(
              style: DefaultTextStyle.of(context).style,
              children: const <TextSpan>[
                TextSpan(
                    text: 'M',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 40,
                      color: Color.fromRGBO(50, 61, 148, 1),
                      fontFamily: 'Fonarto',
                    )),
                TextSpan(
                    text: '-',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 40,
                      fontFamily: 'Fonarto',
                    )),
                TextSpan(
                    text: 'K',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(50, 61, 148, 1),
                      fontSize: 40,
                      fontFamily: 'Fonarto',
                    )),
                TextSpan(
                    text: 'iosk',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 40,
                      fontFamily: 'Fonarto',
                    )),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(5, 6, 8, 16),
            child: Text(
                'Lorem ipsum dolor sit amet,  condimentum a ullamcorper sed, sagittis et magna.',
                style: TextStyle(
                  fontSize: 14,
                )),
          ),
        ],
      ),
    );
  }
}
