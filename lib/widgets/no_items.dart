import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';

class NoItems extends StatelessWidget {
  final String assetName = 'assets/no_items.svg';
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
          height: MediaQuery.of(context).size.height / 1.4,
          child: SvgPicture.asset(assetName, semanticsLabel: 'Acme Logo')),
    );
  }
}
