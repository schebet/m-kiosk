import 'package:flutter/material.dart';
import 'package:read_app/provider/theme_provider.dart';
import 'package:provider/provider.dart';


class ChangeThemeIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context);
    return Switch.adaptive(
      activeColor: Colors.deepOrangeAccent,
        value: themeProvider.isDarkMode, onChanged: (value) {
          final provider = Provider.of<ThemeProvider>(context, listen: false);
          provider.toggleTheme(value);
        });
  }
}
