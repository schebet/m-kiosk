import 'package:flutter/material.dart';

class CartIcon extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, '/cart');
      },
      child: Icon(Icons.shopping_basket),
    );
  }
}