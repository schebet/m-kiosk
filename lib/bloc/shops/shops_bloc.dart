import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:read_app/data/models/shop.dart';
import 'package:read_app/data/repositories/shops_repo.dart';

part 'shops_event.dart';
part 'shops_state.dart';

class ShopsBloc extends Bloc<ShopsEvent, ShopsState> {
  final ShopsRepo shopsRepo;
  ShopsBloc({@required this.shopsRepo}) : super(ShopsInitial()) {
    add(FetchShops());
  }

  ShopsState get initialState => ShopsInitial();

  @override
  Stream<ShopsState> mapEventToState(
    ShopsEvent event,
  ) async* {
    if (event is FetchShops) {
      yield ShopsLoading();
      try {
        final shops = await shopsRepo.getShops();
        yield ShopsLoaded(shops.shops);
      } catch (_) {
        yield ShopsError();
      }
    } 
  }
}
