part of 'shops_bloc.dart';

abstract class ShopsState extends Equatable {
  const ShopsState();

  @override
  List<Object> get props => [];
}

class ShopsInitial extends ShopsState {}

class ShopsLoading extends ShopsState {

}

class ShopsError extends ShopsState {}

class ShopsEmpty extends ShopsState {
  
}

class ShopsLoaded extends ShopsState {
  @override
  List<Shops> get props => shops;
  final List<Shops> shops;

  ShopsLoaded(this.shops) : assert(shops != null);
}