part of 'shop_details_bloc.dart';

abstract class ShopDetailsState extends Equatable {
  const ShopDetailsState();

  @override
  List<Object> get props => [];
}

class ShopDetailsInitial extends ShopDetailsState {}

class ShopDetailsLoading extends ShopDetailsState {}

class ShopDetailsError extends ShopDetailsState {}

class ShopDetailsEmpty extends ShopDetailsState {}

class ShopDetailsLoaded extends ShopDetailsState {
  final Shops shops;
  @override
  get props => [shops];

  ShopDetailsLoaded(this.shops) : assert(shops != null);
}
