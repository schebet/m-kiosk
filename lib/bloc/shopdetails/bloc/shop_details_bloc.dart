import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:read_app/data/models/shop.dart';
import 'package:read_app/data/repositories/shop_details_repo.dart';

part 'shop_details_event.dart';
part 'shop_details_state.dart';

class ShopDetailsBloc extends Bloc<ShopDetailsEvent, ShopDetailsState> {
  final ShopDetailsRepo shopDetailsRepo;
  ShopDetailsBloc({ this.shopDetailsRepo}) : super(ShopDetailsInitial());

  ShopDetailsState get initialState => ShopDetailsInitial();

  @override
  Stream<ShopDetailsState> mapEventToState(
    ShopDetailsEvent event,
  ) async* {
    if (event is FetchShopDetails) {
      yield ShopDetailsLoading();
      try {
        final shops = await shopDetailsRepo.fetchShop(event.id);
        yield ShopDetailsLoaded(shops);
      } catch (_) {
        yield ShopDetailsError();
      }
    } 
  }
}

