part of 'shop_details_bloc.dart';

abstract class ShopDetailsEvent extends Equatable {
  const ShopDetailsEvent();

  @override
  List<Object> get props => [];
}


class FetchShopDetails extends ShopDetailsEvent {

   final id;
   
  const FetchShopDetails(this.id);
}
