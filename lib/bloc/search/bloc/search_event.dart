part of 'search_bloc.dart';

abstract class SearchEvent extends Equatable {
  const SearchEvent();

  @override
  List<Object> get props => [];
}


class FetchSearch extends SearchEvent {

   final term;
   
  const FetchSearch(this.term);
}

