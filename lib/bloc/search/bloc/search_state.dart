part of 'search_bloc.dart';

abstract class SearchState extends Equatable {
  const SearchState();

  @override
  List<Object> get props => [];
}

class SearchInitial extends SearchState {}

class SearchLoading extends SearchState {}

class SearchError extends SearchState {}

class SearchEmpty extends SearchState {}

class SearchLoaded extends SearchState {
  final List<Products> products;
  @override
  List<Object> get props => [products];

  SearchLoaded(this.products) : assert(products != null);
}
