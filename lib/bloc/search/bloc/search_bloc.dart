import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:read_app/data/models/product.dart';
import 'package:read_app/data/models/shop.dart';
import 'package:read_app/data/repositories/search_repo.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final SearchRepo searchRepo;
  SearchBloc({ this.searchRepo}) : super(SearchInitial());

  SearchState get initialState => SearchInitial();

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    if (event is FetchSearch) {
      yield SearchLoading();
      try {
        final products = await searchRepo.searchProducts(event.term);
        yield SearchLoaded(products.products);
      } catch (_) {
        yield SearchError();
      }
    } 
  }
}

