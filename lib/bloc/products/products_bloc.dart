import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:read_app/data/repositories/products_repo.dart';
import 'package:read_app/data/models/product.dart';


part 'products_event.dart';
part 'products_state.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final ProductRepo productsRepo;
  ProductsBloc({ this.productsRepo}) : super(ProductsInitial());

  ProductsState get initialState => ProductsInitial();

  @override
  Stream<ProductsState> mapEventToState(
    ProductsEvent event,
  ) async* {
    if (event is FetchProducts) {
      yield ProductsLoading();
      try {
        final products = await productsRepo.getProducts(event.shopId, event.subCategoryId);
        yield ProductsLoaded(products.products);
      } catch (_) {
        yield ProductsError();
      }
    } 
  }
}
