part of 'products_bloc.dart';

abstract class ProductsEvent extends Equatable {
  const ProductsEvent();

  @override
  List<Object> get props => [];
}


class FetchProducts extends ProductsEvent {

   final shopId;
   final subCategoryId;

  const FetchProducts(this.shopId, this.subCategoryId);
}
