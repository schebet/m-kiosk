part of 'products_bloc.dart';

abstract class ProductsState extends Equatable {
  const ProductsState();

  @override
  List<Object> get props => [];
}

class ProductsInitial extends ProductsState {}

class ProductsLoading extends ProductsState {}

class ProductsError extends ProductsState {}

class ProductsEmpty extends ProductsState {

}

class ProductsLoaded extends ProductsState {
   final List<Products> products;
  @override
   
    List<Object> get props => [products];
  



  ProductsLoaded(this.products) : assert(products != null);
}
