part of 'categories_bloc.dart';

abstract class CategoriesState extends Equatable {
  const CategoriesState();

  @override
  List<Object> get props => [];
}

class CategoriesInitial extends CategoriesState {}

class CategoriesLoading extends CategoriesState {}

class CategoriesError extends CategoriesState {}

class CategoriesEmpty extends CategoriesState {}

class CategoriesLoaded extends CategoriesState {
  final List<Categories> categories;
  @override
  List<Object> get props => [categories];

  CategoriesLoaded(this.categories) : assert(categories != null);
}
