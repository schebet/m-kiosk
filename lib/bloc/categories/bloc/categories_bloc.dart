import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:read_app/data/models/category.dart';
import 'package:read_app/data/repositories/category_list_repo.dart';

part 'categories_event.dart';
part 'categories_state.dart';

class CategoriesBloc extends Bloc<CategoriesEvent, CategoriesState> {
  final CategoryRepo categoriesRepo;
  CategoriesBloc({this.categoriesRepo}) : super(CategoriesInitial()) {
    add(FetchCategories());
  }

  CategoriesState get initialState => CategoriesInitial();

  @override
  Stream<CategoriesState> mapEventToState(
    CategoriesEvent event,
  ) async* {
    if (event is FetchCategories) {
      yield CategoriesLoading();
      try {
        final categories = await categoriesRepo.getCategories();
        yield CategoriesLoaded(categories.categories);
      } catch (_) {
        yield CategoriesError();
      }
    } 
  }
}

