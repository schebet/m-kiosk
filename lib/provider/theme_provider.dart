import 'package:flutter/material.dart';

class ThemeProvider extends ChangeNotifier {
  ThemeMode themeMode = ThemeMode.light;

  bool get isDarkMode => themeMode == ThemeMode.dark;

  void toggleTheme(bool isOn) {
    themeMode = isOn ? ThemeMode.dark : ThemeMode.light;
    notifyListeners();
  }
}

class MyThemes {
  static final darkTheme = ThemeData(
      fontFamily: 'Noto Sans',
      scaffoldBackgroundColor: Colors.grey.shade900,
      primaryColor: Colors.deepOrangeAccent,
      colorScheme: ColorScheme.dark(),
      cardTheme: CardTheme(
        color: Colors.grey.shade800,
      ));
  static final lightTheme = ThemeData(
      fontFamily: 'Noto Sans',
      scaffoldBackgroundColor: Colors.white,
      primaryColor: Colors.deepOrangeAccent,
      cardTheme: CardTheme(
        color: Colors.white
      ),
      colorScheme: ColorScheme.light());
}
