import 'package:flutter/material.dart';
import 'package:read_app/bloc/categories/bloc/categories_bloc.dart';
import 'package:read_app/bloc/products/products_bloc.dart';
import 'package:read_app/bloc/search/bloc/search_bloc.dart';
import 'package:read_app/bloc/shopdetails/bloc/shop_details_bloc.dart';
import 'package:read_app/bloc/shops/shops_bloc.dart';
import 'package:read_app/data/repositories/category_list_repo.dart';
import 'package:read_app/data/repositories/products_repo.dart';
import 'package:read_app/data/repositories/search_repo.dart';
import 'package:read_app/data/repositories/shop_details_repo.dart';
import 'package:read_app/data/repositories/shops_repo.dart';
import 'package:read_app/data/services/HttpService.dart';
import 'package:read_app/services/route.dart';
import 'provider/theme_provider.dart';
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

Future<void> main() async {
  final ShopsRepo shopsRepo = ShopsRepo(ApiService());
  final CategoryRepo categoriesRepo = CategoryRepo(ApiService());
  final ProductRepo productsRepo = ProductRepo(ApiService());
  final SearchRepo searchRepo = SearchRepo(ApiService());
  final ShopDetailsRepo shopDetailsRepo = ShopDetailsRepo(ApiService());
  runApp(MyApp(
      shopsRepo: shopsRepo,
      categoriesRepo: categoriesRepo,
      productsRepo: productsRepo,
      shopDetailsRepo: shopDetailsRepo,
      searchRepo: searchRepo));
}

class MyApp extends StatelessWidget {
  final ShopsRepo shopsRepo;
  final CategoryRepo categoriesRepo;
  final ProductRepo productsRepo;
  final SearchRepo searchRepo;
  final ShopDetailsRepo shopDetailsRepo;

  const MyApp(
      {Key key,
      @required this.shopsRepo,
      @required this.categoriesRepo,
      @required this.productsRepo,
      @required this.shopDetailsRepo,
      @required this.searchRepo})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => ShopsBloc(shopsRepo: shopsRepo)),
        BlocProvider(
            create: (context) =>
                CategoriesBloc(categoriesRepo: categoriesRepo)),
        BlocProvider(
            create: (context) => ProductsBloc(productsRepo: productsRepo)),
        BlocProvider(
            create: (context) => ShopDetailsBloc(shopDetailsRepo: shopDetailsRepo)),
        BlocProvider(create: (context) => SearchBloc(searchRepo: searchRepo)),
      ],
      child: ChangeNotifierProvider(
          create: (context) => ThemeProvider(),
          builder: (context, _) {
            final themeProvider = Provider.of<ThemeProvider>(context);
            return MaterialApp(
              themeMode: themeProvider.themeMode,
              theme: MyThemes.lightTheme,
              darkTheme: MyThemes.darkTheme,
              debugShowCheckedModeBanner: false,
              initialRoute: "/",
              onGenerateRoute: generateRoute,
            );
          }),
    );
  }
}
