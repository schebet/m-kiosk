class Shop {
  List<Shops> shops;

  Shop({this.shops});

  Shop.fromJson(Map<String, dynamic> json) {
    if (json['shops'] != null) {
      shops = new List<Shops>();
      json['shops'].forEach((v) {
        shops.add(new Shops.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.shops != null) {
      data['shops'] = this.shops.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Shops {
  int id;
  String name;
  String updatedAt;
  String createdAt;
  int userId;
  String location;
  String image;
  String description;

  Shops(
      {this.id,
      this.name,
      this.updatedAt,
      this.createdAt,
      this.userId,
      this.location,
      this.image,
      this.description});

  Shops.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    userId = json['user_id'];
    location = json['location'];
    image = json['image'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['user_id'] = this.userId;
    data['location'] = this.location;
    data['image'] = this.image;
    data['description'] = this.description;
    return data;
  }
}
