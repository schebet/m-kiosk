class Product {
  List<Products> products;

  Product({this.products});

  Product.fromJson(Map<String, dynamic> json) {
    if (json['products'] != null) {
      products = new List<Products>();
      json['products'].forEach((v) {
        products.add(new Products.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Products {
  int id;
  String name;
  String image;
  String description;
  int price;
  String updatedAt;
  String createdAt;
  int subCategoryId;
  int shopId;

  Products(
      {this.id,
      this.name,
      this.image,
      this.description,
      this.price,
      this.updatedAt,
      this.createdAt,
      this.subCategoryId,
      this.shopId});

  Products.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
    description = json['description'];
    price = json['price'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    subCategoryId = json['sub_category_id'];
    shopId = json['shop_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    data['description'] = this.description;
    data['price'] = this.price;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['sub_category_id'] = this.subCategoryId;
    data['shop_id'] = this.shopId;
    return data;
  }
}