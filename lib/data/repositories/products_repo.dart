import 'package:read_app/data/models/product.dart';
import 'package:read_app/data/services/HttpService.dart';

class ProductRepo {
  final ApiService apiService;
  ProductRepo(this.apiService, {ProductRepo productsRepo})
      : assert(apiService != null);

  Future<Product> getProducts(shopId, subCategoryId) async {
    return await apiService.getProducts(shopId, subCategoryId);
  }
}
