import 'package:read_app/data/models/product.dart';
import 'package:read_app/data/models/shop.dart';
import 'package:read_app/data/services/HttpService.dart';

class ShopDetailsRepo {
  final ApiService apiService;
  ShopDetailsRepo(this.apiService, {ShopDetailsRepo shopDetailsRepo})
      : assert(apiService != null);

  Future<Shops> fetchShop(id) async {
    return await apiService.fetchShop(id);
  }
}