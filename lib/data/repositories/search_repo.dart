import 'package:read_app/data/models/product.dart';
import 'package:read_app/data/services/HttpService.dart';

class SearchRepo {
  final ApiService apiService;
  SearchRepo(this.apiService, {SearchRepo productsRepo})
      : assert(apiService != null);

  Future<Product> searchProducts(term) async {
    return await apiService.searchProducts(term);
  }
}
