import 'package:read_app/data/models/category.dart';
import 'package:read_app/data/models/shop.dart';
import 'package:read_app/data/services/HttpService.dart';
class CategoryRepo{

    final ApiService apiService;
  CategoryRepo(this.apiService, {CategoryRepo categoryListRepo}) : assert(apiService != null);

  Future<CategoryList> getCategories() async {
    return await apiService.getCategories();
  }

}