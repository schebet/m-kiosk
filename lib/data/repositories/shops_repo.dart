import 'package:read_app/data/models/shop.dart';
import 'package:read_app/data/services/HttpService.dart';
class ShopsRepo{

    final ApiService apiService;
  ShopsRepo(this.apiService, {ShopsRepo shopsRepo}) : assert(apiService != null);

  Future<Shop> getShops() async {
    return await apiService.getShops();
  }

}