import 'dart:convert';
import 'package:read_app/data/models/category.dart';
import 'package:read_app/data/models/shop.dart';
import 'package:http/http.dart' as http;
import 'package:read_app/data/models/product.dart';

class ApiService {
  Future<Shop> getShops() async {
    var _url = Uri.parse('http://192.168.1.9:3333/shops');

    var response = await http.get(_url);
    var body = response.body;
    var json = jsonDecode(body);
    print(body);

    return Shop.fromJson(jsonDecode(body));
  }

  Future<CategoryList> getCategories() async {
    var _url =
        Uri.parse('http://192.168.1.9:3333/categories/category/subcategories');

    var response = await http.get(_url);
    var body = response.body;
    var json = jsonDecode(body);
    print(body);

    return CategoryList.fromJson(jsonDecode(body));
  }

  Future<Product> getProducts(shopId, subCategoryId) async {
    var x = shopId;
    var y = subCategoryId;
    var _url = Uri.parse('http://192.168.1.9:3333/products/$y/$x');

    var response = await http.get(_url);
    var body = response.body;
    var json = jsonDecode(body);
    print(body);

    return Product.fromJson(jsonDecode(body));
  }
  Future<Shops> fetchShop(shopId) async {
    var x = shopId;
 
    var _url = Uri.parse('http://192.168.1.9:3333/shops/$x');

    var response = await http.get(_url);
    var body = response.body;
    var json = jsonDecode(body);
    print(body);

    return Shops.fromJson(jsonDecode(body));
  }

  Future<Product> searchProducts(y) async {
    try {
      var _url = Uri.parse('http://192.168.1.9:3333/products/search');
      var response = await http.post(_url,
          headers: {
            "Accept": "multipart/formdata",
            "Content-Type": "application/x-www-form-urlencoded"
          },
          body: {"term": y},
          encoding: Encoding.getByName("utf-8"));

      var body = response.body;
      print(body);

      return Product.fromJson(jsonDecode(body));
    } catch (e) {
      print(e);
    }
  }
}
