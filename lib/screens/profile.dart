import 'package:flutter/material.dart';
import 'package:read_app/widgets/appbar.dart';
import 'package:read_app/widgets/change_theme.dart';
import 'package:read_app/widgets/logo.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: SafeArea(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          CustomAppBar(),
          Logo(),
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 8, 8, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Profile',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                    )),
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            child: ListView(
              children: [
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text('Stacey Ann'),
                  trailing: Icon(Icons.edit),
                ),
                ListTile(
                  leading: Icon(Icons.room_preferences),
                  title: Text('Change Theme'),
                  trailing: ChangeThemeIcon(),
                ),
              ],
            ),
          )
        ]),
      ),
    )));
  }
}
