import 'package:flutter/material.dart';

class Favorites extends StatefulWidget {
  @override
  _FavoritesState createState() => _FavoritesState();
}

class _FavoritesState extends State<Favorites> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              child: Card(
                elevation: 1,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      ListTile(
                          leading: GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(context, '/');
                            },
                            child: Icon(
                              Icons.timeline,
                              color: Colors.deepOrangeAccent,
                            ),
                          ),
                          trailing: Icon(
                            Icons.search,
                            color: Colors.deepOrangeAccent,
                          )),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 8, 8, 0),
                        child: Text('App Name',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 25,
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(18, 2, 8, 8),
                        child: Text('Favorites',
                            style: TextStyle(
                              fontSize: 25,
                            )),
                      ),
                    ]),
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0),
                  ),
                ),
              ),
            ),
            Expanded(
              child: ListView.separated(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: 10,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    padding: EdgeInsets.all(3.0),
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      child: ListTile(
                        leading: FlutterLogo(),
                        trailing: Icon(
                          Icons.favorite,
                          color: Colors.redAccent,
                        ),
                        isThreeLine: true,
                        title: Text(
                          'Title',
                        ),
                        subtitle: Text('Description'),
                      ),
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    height: 1,
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
