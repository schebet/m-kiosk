import 'package:flutter/material.dart';
import 'package:read_app/bloc/products/products_bloc.dart';
import 'package:read_app/data/models/category.dart';
import 'package:read_app/data/models/shop.dart';
import 'package:read_app/widgets/appbar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Products extends StatefulWidget {
  final Shops shops;
  final SubCategories subCategories;
  const Products({Key key, @required this.shops, @required this.subCategories})
      : super(key: key);
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<ProductsBloc>(context)
        .add(FetchProducts(widget.shops.id, widget.subCategories.id));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          child: BlocListener<ProductsBloc, ProductsState>(
              listener: (context, ProductsState state) {},
              child: BlocBuilder<ProductsBloc, ProductsState>(
                  builder: (context, ProductsState state) {
                if (state is ProductsLoading) {
                  return Center(child: CircularProgressIndicator());
                } else if (state is ProductsLoaded) {
                  return SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomAppBar(),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(15, 15, 8, 0),
                            child: Text(widget.shops.name.toUpperCase(),
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 20,
                                  color: Color.fromRGBO(50, 61, 148, 1),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(15, 8, 8, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('PRODUCTS',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15,
                                    )),
                              ],
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height,
                            child: GridView.builder(
                                gridDelegate:
                                    SliverGridDelegateWithMaxCrossAxisExtent(
                                        maxCrossAxisExtent: 200,
                                        mainAxisExtent: 200,
                                        childAspectRatio: 2 / 2,
                                        crossAxisSpacing: 10,
                                        mainAxisSpacing: 10),
                                itemCount: state.products.length,
                                itemBuilder: (BuildContext ctx, index) {
                                  return Container(
                                    child: Card(
                                      elevation: 0,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              Navigator.pushNamed(
                                                  context, '/product',
                                                  arguments:
                                                      state.products[index]);
                                            },
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height /
                                                  8,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                  image: NetworkImage(state
                                                      .products[index].image),
                                                  fit: BoxFit.fitHeight,
                                                  alignment:
                                                      Alignment.topCenter,
                                                ),
                                              ),
                                            ),
                                          ),
                                          ListTile(
                                              title: Text(
                                                state.products[index].name,
                                                style: TextStyle(fontSize: 12),
                                              ),
                                              subtitle: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      'KES ' +
                                                          state.products[index]
                                                              .price
                                                              .toString(),
                                                      style: TextStyle(
                                                          fontSize: 11),
                                                    ),
                                                    Icon(Icons.shopping_cart),
                                                  ])),
                                        ],
                                      ),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                    ),
                                  );
                                }),
                          ),
                        ],
                      ),
                    ),
                  );
                } else if(state is ProductsEmpty){
                  Text('data', style: TextStyle(fontSize: 49),);
                }
                return Center(child: CircularProgressIndicator());
              }))),
    );
  }
}
