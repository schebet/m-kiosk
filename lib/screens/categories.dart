import 'package:flutter/material.dart';
import 'package:read_app/widgets/appbar.dart';
import 'package:read_app/widgets/change_theme.dart';

class Categories extends StatefulWidget {
  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomAppBar(),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 15, 8, 0),
                  child: Text('All Categories',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 20,
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(18, 6, 8, 16),
                  child: Text(
                      'Lorem ipsum dolor sit amet,  condimentum a ullamcorper sed, sagittis et magna.',
                      style: TextStyle(
                        fontSize: 16,
                      )),
                ),
                Container(
                    height: MediaQuery.of(context).size.height,
                    child: ListView.builder(
                      itemCount: 6,
                      itemBuilder: (context, index) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(15, 8, 8, 0),
                              child: Text('Category 1',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14,
                                  )),
                            ),
                            Container(
                              height: 150,
                              child: GridView.builder(
                                  scrollDirection: Axis.horizontal,
                                  gridDelegate:
                                      SliverGridDelegateWithMaxCrossAxisExtent(
                                          maxCrossAxisExtent: 150,
                                          childAspectRatio: 2 / 2.5,
                                          crossAxisSpacing: 20,
                                          mainAxisSpacing: 20),
                                  itemCount: 10,
                                  itemBuilder: (BuildContext ctx, index) {
                                    return Container(
                                      child: GestureDetector(
                                        behavior: HitTestBehavior.opaque,
                                        onTap: () {
                                          Navigator.pushNamed(
                                              context, '/products');
                                        },
                                        child: Card(
                                          elevation: 0,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    6,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                    image: NetworkImage(
                                                        "https://sb.kaleidousercontent.com/800x533/f8d936ad1a/products_no_background.png"),
                                                    fit: BoxFit.fitWidth,
                                                    alignment:
                                                        Alignment.topCenter,
                                                  ),
                                                ),
                                              ),
                                              ListTile(
                                                title: Text('One-line '),
                                              ),
                                            ],
                                          ),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15)),
                                        ),
                                      ),
                                    );
                                  }),
                            ),
                          ],
                        );
                      },
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
