import 'package:flutter/material.dart';
import 'package:read_app/bloc/shops/shops_bloc.dart';
import 'package:read_app/widgets/appbar.dart';
import 'package:read_app/widgets/change_theme.dart';
import 'package:read_app/widgets/logo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Stores extends StatefulWidget {
  @override
  _StoresState createState() => _StoresState();
}

class _StoresState extends State<Stores> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: BlocListener<ShopsBloc, ShopsState>(
          listener: (context, ShopsState state) {},
          child: BlocBuilder<ShopsBloc, ShopsState>(
              builder: (context, ShopsState state) {
            if (state is ShopsLoading) {
              return Center(child: CircularProgressIndicator());
            } else if (state is ShopsLoaded) {
              return SafeArea(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomAppBar(),
                      Logo(),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 8, 8, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('ALL STORES',
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(50, 61, 148, 1),
                                  fontSize: 14,
                                )),
                          ],
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height,
                        child: GridView.builder(
                            gridDelegate:
                                SliverGridDelegateWithMaxCrossAxisExtent(
                                    maxCrossAxisExtent: 200,
                                    mainAxisExtent: 200,
                                    childAspectRatio: 2 / 2,
                                    crossAxisSpacing: 10,
                                    mainAxisSpacing: 10),
                            itemCount: state.props.length,
                            itemBuilder: (BuildContext ctx, index) {
                              return Container(
                                child: GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    Navigator.pushNamed(context, '/store',
                                        arguments: state.shops[index]);
                                  },
                                  child: Card(
                                    elevation: 2,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              5.7,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image: NetworkImage(
                                                  state.shops[index].image),
                                              alignment: Alignment.topCenter,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            state.props[index].name
                                                .toUpperCase(),
                                            style: TextStyle(
                                              fontSize: 10,
                                              color: Color.fromRGBO(
                                                  50, 61, 148, 1),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                  ),
                                ),
                              );
                            }),
                      ),
                    ],
                  ),
                ),
              );
            }
            return Center(child: CircularProgressIndicator());
          }),
        ),
      ),
    );
  }
}
