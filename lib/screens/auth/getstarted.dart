import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:read_app/widgets/logo.dart';

class GetStarted extends StatelessWidget {
  final String assetName = 'assets/illustration.svg';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
        SizedBox(
          height: MediaQuery.of(context).size.height/1.4,
          child: SvgPicture.asset(assetName, semanticsLabel: 'Acme Logo')),
    Padding(
      padding: const EdgeInsets.fromLTRB(15, 0, 8, 0),
      child: RichText(
        text: TextSpan(
          style: DefaultTextStyle.of(context).style,
          children: const <TextSpan>[
            TextSpan(
                text: 'M',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 40,
                  color: Color.fromRGBO(50, 61, 148, 1),
                  fontFamily: 'Fonarto',
                )),
            TextSpan(
                text: '-',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 40,
                  fontFamily: 'Fonarto',
                )),
            TextSpan(
                text: 'K',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(50, 61, 148, 1),
                  fontSize: 40,
                  fontFamily: 'Fonarto',
                )),
            TextSpan(
                text: 'iosk',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 40,
                  fontFamily: 'Fonarto',
                )),
          ],
        ),
      ),
    ),
        Center(
          child: ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/auth');
              },
              
              child: Text(
                'GET STARTED',
                style: TextStyle(color: Colors.white),
              ),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                      Color.fromRGBO(50, 61, 148, 1)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          side: BorderSide())))),
        )
      ]),
    );
  }
}
