import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:read_app/widgets/logo.dart';

class Login extends StatefulWidget {
  final Function toggleview;
  const Login({
    this.toggleview,
  });

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
            child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RichText(
                  text: TextSpan(
                    style: DefaultTextStyle.of(context).style,
                    children: const <TextSpan>[
                      TextSpan(
                          text: 'M',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 40,
                            color: Color.fromRGBO(50, 61, 148, 1),
                            fontFamily: 'Fonarto',
                          )),
                      TextSpan(
                          text: '-',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 40,
                            fontFamily: 'Fonarto',
                          )),
                      TextSpan(
                          text: 'K',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Color.fromRGBO(50, 61, 148, 1),
                            fontSize: 40,
                            fontFamily: 'Fonarto',
                          )),
                      TextSpan(
                          text: 'iosk',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 40,
                            fontFamily: 'Fonarto',
                          )),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextFormField(
                  decoration: new InputDecoration(
                    labelText: "Email",
                    labelStyle: TextStyle(color: Colors.black54),
                    // fillColor: Color.fromRGBO(50, 61, 148, 1),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(
                        color: Color.fromRGBO(50, 61, 148, 1),
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(
                        color: Color.fromRGBO(50, 61, 148, 1),
                        width: 2.0,
                      ),
                    ),
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                      borderSide: new BorderSide(color: Colors.black),
                    ),
                  ),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Email cannot be empty";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                  style:
                      new TextStyle(fontFamily: "Poppins", color: Colors.black),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextFormField(
                  obscureText: true,
                  decoration: new InputDecoration(
                    labelText: "Password",
                    labelStyle: TextStyle(color: Colors.black54),
                    // fillColor: Color.fromRGBO(50, 61, 148, 1),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(
                        color: Color.fromRGBO(50, 61, 148, 1),
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(
                        color: Color.fromRGBO(50, 61, 148, 1),
                        width: 2.0,
                      ),
                    ),
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                      borderSide: new BorderSide(color: Colors.black),
                    ),
                  ),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Password cannot be empty";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                  style:
                      new TextStyle(fontFamily: "Poppins", color: Colors.black),
                ),
              ),

              ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/screenwrap');
                  },
                  child: Text(
                    'LOGIN',
                    style: TextStyle(color: Colors.white),
                  ),
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                          Color.fromRGBO(50, 61, 148, 1)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              side: BorderSide())))),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RichText(
                  text: TextSpan(
                    children:  <TextSpan>[
                      TextSpan(
                          text: "Don't have an account?",
                          style: TextStyle(color: Colors.black)),
                      TextSpan(
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              widget.toggleview();
                            },
                          text: ' Register',
                          style: TextStyle(
                            color: Color.fromRGBO(50, 61, 148, 1),
                          )),
                    ],
                  ),
                ),
              ),
            ],
          ),
        )));
  }
}
