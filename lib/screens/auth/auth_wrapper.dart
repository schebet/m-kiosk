import 'package:flutter/material.dart';
import 'package:read_app/screens/auth/login.dart';
import 'package:read_app/screens/auth/register.dart';



class AuthWrapper extends StatefulWidget {
  @override
  _AuthWrapperState createState() => _AuthWrapperState();
}

class _AuthWrapperState extends State<AuthWrapper> {
  bool showLogin = false;

  void toggleView(){
    setState(() {
      
      showLogin=!showLogin;
    });
  }
  @override
  Widget build(BuildContext context) {
    if (showLogin) {
      return Login(toggleview: toggleView);
    } else {
      return Register(toggleview :toggleView);
    }
  }
}
