import 'package:flutter/material.dart';
import 'package:read_app/widgets/appbar.dart';
import 'package:read_app/widgets/change_theme.dart';

class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomAppBar(),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 8, 8, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('My Cart',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                          )),
                    ],
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height/2,
                                  child: ListView.builder(
                    itemCount: 5,
                    itemBuilder: (context, index) {
                      return Row(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width / 3,
                            height: MediaQuery.of(context).size.height / 10,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                    "https://lh3.googleusercontent.com/proxy/QuktoBLEi5e0Ana-d7Cwh7HvOVOy47q97Bf-CFL8oPoGa1tyIGg9mQ1jUzQLO3CM2B83qHHjt1Hi5t9eAMnoVaDbz09fRiYSDzGfRXUppkyVWpvJ4qiL2-q18n_wgde2ww"),
                                // alignment: Alignment.topCenter,
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 1.5,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'BACKBEAT SGSJS 2 2016',
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  Text(
                                    'KES 2000',
                                    style: TextStyle(fontSize: 9),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          Card(child: Icon(Icons.remove)),
                                          Text('1'),
                                          Card(child: Icon(Icons.add)),
                                          Card(child: Icon(Icons.delete)),
                                        ]),
                                  )
                                ]),
                          )
                        ],
                      );
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 14, 8, 0),
                  child: Text('Delivery Location',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 11,
                      )),
                ),
                ListTile(
                  leading: Icon(Icons.location_city_outlined),
                  title: Text('Kings Millenium',
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 9,
                      )),
                  subtitle: Text('Apartment No B52',
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 9,
                      )),
                  trailing: Icon(Icons.edit),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 14, 8, 0),
                  child: Text('Payment Method',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 11,
                      )),
                ),
                ListTile(
                  leading: Icon(Icons.credit_card),
                  title: Text('VISA',
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 9,
                      )),
                  subtitle: Text('****-0968',
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 9,
                      )),
                  trailing: Icon(Icons.edit),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 14, 8, 0),
                  child: Text('Order Information',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 11,
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 14, 10, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Subtotal',
                          style: TextStyle(
                            fontWeight: FontWeight.w100,
                            fontSize: 9,
                          )),
                      Text('KES 2000',
                          style: TextStyle(
                            fontWeight: FontWeight.w100,
                            fontSize: 9,
                          )),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 14, 10, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Shipping Fee',
                          style: TextStyle(
                            fontWeight: FontWeight.w100,
                            fontSize: 9,
                          )),
                      Text('KES 200',
                          style: TextStyle(
                            fontWeight: FontWeight.w100,
                            fontSize: 9,
                          )),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 14, 10, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Total',
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 11,
                          )),
                      Text('KES 200',
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 11,
                          )),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: ElevatedButton(
                        onPressed: () {},
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(50, 8, 50, 10),
                          child: Text('CHECKOUT'),
                        ),
                        style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                    side: BorderSide())))),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
