import 'package:flutter/material.dart';
import 'package:read_app/bloc/shopdetails/bloc/shop_details_bloc.dart';
import 'package:read_app/data/models/product.dart' as product;
import 'package:read_app/widgets/appbar.dart';
import 'package:read_app/widgets/change_theme.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Product extends StatefulWidget {
  final product.Products products;

  const Product({Key key, @required this.products}) : super(key: key);
  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<ShopDetailsBloc>(context).add(FetchShopDetails(1));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomAppBar(),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 8, 8, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Product',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                          )),
                    ],
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 2,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 10,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(widget.products.image),
                        fit: BoxFit.fitWidth,
                        alignment: Alignment.topCenter,
                      ),
                    ),
                  ),
                ),
                ListTile(
                  title: Text(
                    widget.products.name.toUpperCase(),
                  ),
                  subtitle: Text(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
                    style: TextStyle(fontSize: 10),
                  ),
                ),
                BlocListener<ShopDetailsBloc, ShopDetailsState>(
                    listener: (context, ShopDetailsState state) {},
                    child: BlocBuilder<ShopDetailsBloc, ShopDetailsState>(
                        builder: (context, ShopDetailsState state) {
                      if (state is ShopDetailsLoading) {
                        return Center(child: CircularProgressIndicator());
                      } else if (state is ShopDetailsLoaded) {
                        return ListTile(
                          title: Text(state.shops.name),
                          subtitle: Text(
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
                            style: TextStyle(fontSize: 10),
                          ),
                        );
                      }
                      return Center(child: CircularProgressIndicator());
                    })),
                ListTile(
                  subtitle: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'KES ' + widget.products.price.toString(),
                        style: TextStyle(fontSize: 13),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            '1',
                            style: TextStyle(fontSize: 14),
                          ),
                          Card(child: Icon(Icons.add)),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: ElevatedButton(
                        onPressed: () {},
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(50, 8, 50, 10),
                          child: Text('ADD TO CART'),
                        ),
                        style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                    side: BorderSide())))),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
