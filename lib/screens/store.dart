import 'package:flutter/material.dart';
import 'package:read_app/bloc/categories/bloc/categories_bloc.dart';
import 'package:read_app/data/models/shop.dart';
import 'package:read_app/widgets/appbar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Store extends StatefulWidget {
  final Shops shops;

  const Store({Key key, @required this.shops}) : super(key: key);
  @override
  _StoreState createState() => _StoreState();
}

class _StoreState extends State<Store> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: BlocListener<CategoriesBloc, CategoriesState>(
        listener: (context, CategoriesState state) {},
        child: BlocBuilder<CategoriesBloc, CategoriesState>(
            builder: (context, CategoriesState state) {
          if (state is CategoriesLoading) {
            return Center(child: CircularProgressIndicator());
          } else if (state is CategoriesLoaded) {
            return SafeArea(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomAppBar(),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 8, 0),
                      child: Text(widget.shops.name.toUpperCase() ?? "",
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 20,
                              color: Color.fromRGBO(50, 61, 148, 1))),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(18, 6, 8, 16),
                      child: Text(widget.shops.description,
                          style: TextStyle(
                            fontSize: 16,
                          )),
                    ),
                    Container(
                        height: MediaQuery.of(context).size.height,
                        child: ListView.builder(
                          itemCount: state.categories.length,
                          itemBuilder: (context, i) {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(15, 8, 8, 0),
                                  child: Text(
                                      state.categories[i].name.toUpperCase(),
                                      style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(50, 61, 148, 1),
                                        fontSize: 14,
                                      )),
                                ),
                                Container(
                                  height: 150,
                                  child: GridView.builder(
                                      scrollDirection: Axis.horizontal,
                                      gridDelegate:
                                          SliverGridDelegateWithMaxCrossAxisExtent(
                                              maxCrossAxisExtent: 150,
                                              childAspectRatio: 2 / 2.5,
                                              crossAxisSpacing: 20,
                                              mainAxisSpacing: 20),
                                      itemCount: state
                                          .categories[i].subCategories.length,
                                      itemBuilder: (BuildContext ctx, index) {
                                        return Container(
                                          child: GestureDetector(
                                            behavior: HitTestBehavior.opaque,
                                            onTap: () async {
                                              print(widget.shops.id);
                                              print(state.categories[i]
                                                  .subCategories[index].id);
                                              Navigator.pushNamed(
                                                  context, '/products',
                                                  arguments: {
                                                    "shops": widget.shops,
                                                    "sub": state.categories[i]
                                                        .subCategories[index]
                                                  });
                                            },
                                            child: Card(
                                              elevation: 0,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            6,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image: NetworkImage(
                                                            state
                                                                .categories[i]
                                                                .subCategories[
                                                                    index]
                                                                .image),
                                                        fit: BoxFit.fitWidth,
                                                        alignment:
                                                            Alignment.topCenter,
                                                      ),
                                                    ),
                                                  ),
                                                  ListTile(
                                                    title: Text(
                                                        state
                                                            .categories[i]
                                                            .subCategories[
                                                                index]
                                                            .name
                                                            .toUpperCase(),
                                                        style: TextStyle(
                                                            fontSize: 10)),
                                                  ),
                                                ],
                                              ),
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15)),
                                            ),
                                          ),
                                        );
                                      }),
                                ),
                              ],
                            );
                          },
                        )),
                  ],
                ),
              ),
            );
          }
          return Center(child: CircularProgressIndicator());
        }),
      ),
    ));
  }
}
