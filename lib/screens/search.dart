import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:read_app/bloc/search/bloc/search_bloc.dart';
import 'package:read_app/widgets/no_items.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  var term;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(children: [
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black12,
                  width: 1,
                ),
              ),
              child: ListTile(
                leading: BackButton(),
                title: TextField(
                  onChanged: (value) {
                    setState(() {
                      term = value;
                    });
                  },
                  decoration: InputDecoration(
                      disabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      hintText: 'Search'),
                ),
                trailing: GestureDetector(
                    onTap: () {
                  
                      BlocProvider.of<SearchBloc>(context)
                          .add(FetchSearch(term));
                    },
                    child: Icon(Icons.search)),
              ),
            ),
            Container(
                height: MediaQuery.of(context).size.height,
                child: BlocListener<SearchBloc, SearchState>(
                    listener: (context, SearchState state) {},
                    child: BlocBuilder<SearchBloc, SearchState>(
                        builder: (context, SearchState state) {
                      if (state is SearchLoading) {
                        return Center(child: CircularProgressIndicator());
                      } else if (state is SearchLoaded) {
                        return ListView.builder(
                            padding: const EdgeInsets.all(8),
                            itemCount: state?.products?.length?? 0,
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector( 
                                onTap: () {
                                  Navigator.pushNamed(context, '/product',
                                      arguments: state.products[index]);
                                },
                                child: ListTile(
                                  title: Text(state.products[index].name),
                                  subtitle: Text(
                                      state.products[index].price.toString()),
                                ),
                              );
                            });
                      } else if (state is SearchEmpty) {
                        return Text('Nad');
                        // NoItems();
                      }
                      return SizedBox();
                    })))
          ]),
        ),
      ),
    );
  }
}
