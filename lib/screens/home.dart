import 'package:flutter/material.dart';
import 'package:read_app/bloc/shops/shops_bloc.dart';
import 'package:read_app/widgets/appbar.dart';
import 'package:read_app/widgets/change_theme.dart';
import 'package:read_app/data/services/HttpService.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomAppBar(),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 15, 8, 0),
                  child: RichText(
                    text: TextSpan(
                      style: DefaultTextStyle.of(context).style,
                      children: const <TextSpan>[
                        TextSpan(
                            text: 'M',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 40,
                              color: Color.fromRGBO(50, 61, 148, 1),
                              fontFamily: 'Fonarto',
                            )),
                        TextSpan(
                            text: '-',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 40,
                              fontFamily: 'Fonarto',
                            )),
                        TextSpan(
                            text: 'K',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Color.fromRGBO(50, 61, 148, 1),
                              fontSize: 40,
                              fontFamily: 'Fonarto',
                            )),
                        TextSpan(
                            text: 'iosk',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 40,
                              fontFamily: 'Fonarto',
                            )),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(18, 6, 8, 16),
                  child: Text(
                      'Lorem ipsum dolor sit amet,  condimentum a ullamcorper sed, sagittis et magna.',
                      style: TextStyle(
                        fontSize: 14,
                      )),
                ),
                Padding(
                    padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
                    child: InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, '/search');
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 40,
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(Icons.search),
                            )
                          ],
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                    )),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 8, 8, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Available Shops',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                          )),
                      ElevatedButton(
                          onPressed: () async {
                            // await ApiService.shops();
                            Navigator.pushNamed(context, '/stores');
                          },
                          child: Text(
                            'See All',
                            style: TextStyle(color: Colors.white),
                          ),
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  Color.fromRGBO(50, 61, 148, 1)),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                      side: BorderSide())))),
                    ],
                  ),
                ),
                Container(
                    height: MediaQuery.of(context).size.height / 2.5,
                    child: BlocListener<ShopsBloc, ShopsState>(
                        listener: (context, ShopsState state) {},
                        child: BlocBuilder<ShopsBloc, ShopsState>(
                            builder: (context, ShopsState state) {
                          if (state is ShopsLoading) {
                            return Center(child: CircularProgressIndicator());
                          } else if (state is ShopsLoaded) {
                            return GridView.builder(
                                physics: const NeverScrollableScrollPhysics(),
                                gridDelegate:
                                    SliverGridDelegateWithMaxCrossAxisExtent(
                                        maxCrossAxisExtent: 130,
                                        mainAxisExtent: 150,
                                        childAspectRatio: 2 / 2,
                                        crossAxisSpacing: 10,
                                        mainAxisSpacing: 10),
                                itemCount: 6,
                                itemBuilder: (BuildContext ctx, index) {
                                  return Container(
                                    child: GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () async {
                                        Navigator.pushNamed(context, '/store',
                                            arguments: state.props[index]);
                                      },
                                      child: Card(
                                        elevation: 1,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height /
                                                  10,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                  image: NetworkImage(
                                                      state.shops[index].image),
                                                  fit: BoxFit.fitWidth,
                                                  alignment:
                                                      Alignment.topCenter,
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(
                                                  state.props[index].name
                                                      .toUpperCase(),
                                                  style:
                                                      TextStyle(fontSize: 10)),
                                            )
                                          ],
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      ),
                                    ),
                                  );
                                });
                          }
                          return Center(child: CircularProgressIndicator());
                        }))),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
